console.log('=============');
console.log('Tugas 6');
console.log('=============');
console.log('Soal No. 1 - Array to Object');
console.log('=============');
function hitung_umur(umur){
    var umur_akhir = 0
    var today = new Date();
    var thisYear = today.getFullYear();
    if (!umur){
        umur_akhir = "Invalid Birth Year"
    } else if(umur < 0 || umur >2020){
        umur_akhir = "Invalid Birth Year"
    } else {
        umur_akhir = thisYear - umur
    }
    return umur_akhir
}

function arrayToObject(arr){
    var array_length = arr.length
    var personObj ={}
    var personObjUtama ={}
    if(array_length > 0){
    for (var i = 0; i < array_length; i++){
        personObj = {
        firstName : arr[i][0],
        lastName: arr[i][1],
        gender: arr[i][2],
        age: hitung_umur(arr[i][3])
        }
        var awalan = (i+1) +" "+ arr[i][0]+" "+ arr[i][1];
        personObjUtama[awalan]= personObj
    }      
        console.log(personObjUtama)
    } else {
        console.log("Input kurang tepat")
    }
}

console.log('Output: ');
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2)
console.log('=============');
console.log('Soal No. 2 - Shopping Time');
console.log('=============');

function shoppingTime(memberId, money) {
	var result =''
	var listPurchased = []
	var first_money = money
	
	if(!memberId){
		result = 'Mohon maaf, toko X hanya berlaku untuk member saja'
	} else if(money < 50000){
		result ='Mohon maaf, uang tidak cukup'
	} else {
		var shoopingObj = {}	
		while (money > 49999){
			if(money >= 1500000){
				listPurchased.push('Sepatu Stacattu')
				money = money - 1500000
			}else if(money >= 500000){
				listPurchased.push('Baju Zoro')
				money = money - 500000
			}else if(money >= 250000){
				listPurchased.push('Baju H&N')
				money = money - 250000
			}else if(money >= 175000){
				listPurchased.push('Sweater Uniklooh')
				money = money - 175000
			}else{
				listPurchased.push('Casing Handphone')
				money = money - 50000
				break;
			}			
		}
		shoopingObj['memberId']= memberId
		shoopingObj['money']= first_money
		shoopingObj['listPurchased']= listPurchased
		shoopingObj['changeMoney']= money
		result = shoopingObj
	}
	return result
}
	
// TEST CASES
console.log('Output: ');
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); 
console.log(shoppingTime('234JdhweRxa53', 500000)); 
console.log(shoppingTime());
console.log('=============');
console.log('Soal No. 3 Naik Angkot');
console.log('=============');
function RubahHuruf(huruf){
	var angka = 0
	switch(huruf){
		case 'a' : {angka=1; break;}
		case 'b' : {angka=2; break;}
		case 'c' : {angka=3; break;}
		case 'd' : {angka=4; break;}
		case 'e' : {angka=5; break;}
		case 'f' : {angka=6; break;}		
		default : {angka=1; break;}
	}
	return angka
}

function HitungOngkosStatik(awal, tujuan){
	var harga = 0
	var angkaMulai = RubahHuruf(awal.toLowerCase())
	var angkaAkhir = RubahHuruf(tujuan.toLowerCase())
	harga = (angkaAkhir - angkaMulai )* 2000
	return harga
}

function naikAngkot(arrPenumpang) {
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  var arr_length = arrPenumpang.length
  var naikAngkotArr = []
  var result =''
  if(!arrPenumpang){
	result = ''
  }else{	
	if(arr_length > 0){
		for (var i = 0; i < arr_length; i++){
			var penumpangAngkotObj = {}
			penumpangAngkotObj['penumpang']= arrPenumpang[i][0]
			penumpangAngkotObj['naikDari']= arrPenumpang[i][1]
			penumpangAngkotObj['tujuan']= arrPenumpang[i][2]
			penumpangAngkotObj['bayar']= HitungOngkosStatik(arrPenumpang[i][1],arrPenumpang[i][2])
			naikAngkotArr.push(penumpangAngkotObj)
		}
		
		result = naikAngkotArr 
	}else {
		result = naikAngkotArr
	}
  }
  return result
}
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']])); 
console.log(naikAngkot([])); //[]