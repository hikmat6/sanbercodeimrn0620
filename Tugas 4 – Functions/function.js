console.log('=============');
console.log('Tugas 4');
console.log('=============');
console.log('Soal No 1 - function teriak()');
console.log('=============');
function teriak() {
  var string = prompt("Masukkan Nama Anda").toLowerCase();
  return string
}
console.log('Output: ');
console.log(teriak()) // "Halo Sanbers!"
console.log('=============');
console.log('Soal No 2 - function multiply()');
console.log('=============');
function kalikan(a,b) {
  var result = a * b;
  return result
}
 
var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log('Output: ');
console.log(hasilKali) 

console.log('=============');
console.log('Soal No 3 - function introduce()');
console.log('=============');
function introduce(a,b,c,d) {  
  var stringKenalan = "Nama saya "+ a +", umur saya "+ b +" tahun, alamat saya di "+ c +", dan saya punya hobby yaitu "+ d +"!";
  return stringKenalan
}
 
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log('Output: ');
console.log(perkenalan) 