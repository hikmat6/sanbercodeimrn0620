console.log('=============')
console.log('Tugas 5')
console.log('=============')
console.log('Soal No 1 - (Range)()')
console.log('=============')
function range(startNum, finishNum) {
  if(!startNum ||!finishNum){
	var number = -1
  }else if(!Number.isInteger(startNum) ||!Number.isInteger(finishNum)){
	var number = -1
  }else {
	var number = []
	while(startNum != finishNum) { 
	  number.push(startNum)
	  if(startNum < finishNum){
		startNum++; 
	  }else{
		startNum--;
	  }
	  //last_checked
	  if(startNum == finishNum){
		number.push(startNum)
	  }
	}
  }
  return number
}
console.log('Output: ')
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 
console.log('=============')
console.log('Soal No 2 -  (Range with Step)()')
console.log('=============')
function rangeWithStep(startNum, finishNum, step) {
  if(!startNum ||!finishNum||!step){
	var number = -1
  }else if(!Number.isInteger(startNum) ||!Number.isInteger(finishNum) ||!Number.isInteger(step)){
	var number = -1
  }else {
	  var number = []
	  if(startNum < finishNum){
		for(var deret = startNum; deret <= finishNum; deret += step) {
		  number.push(deret)
		}
	  }else{
		for(var deret = startNum; deret >= finishNum; deret -= step) {
		  number.push(deret)
		} 
	  }	
  }
  return number
}
console.log('Output: ')
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
console.log('=============')
console.log('Soal No 3 -  (Sum of Range)()')
console.log('=============')
function sum(startNum, finishNum, step) {
  
  var barisderet = 0;
  var number = 0;
  
  //check bilangan deret
  if (step){
	barisderet = step
  }else{
    barisderet = 1
  }
  
  //logic
  if(startNum && !finishNum && !step){
	number = startNum
  }else if(!finishNum){
	number = 0
  }else {	
	  if(startNum < finishNum){
		for(var deret = startNum; deret <= finishNum; deret += barisderet) {
		  number += deret;
		}
	  }else{
		for(var deret = startNum; deret >= finishNum; deret -= barisderet) {
		  number += deret;
		} 
	  }	
  }
  return number
}
console.log('Output: ')
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0
console.log('=============')
console.log('=============')
console.log('Soal No. 4 - (Array Multidimensi)()')
console.log('=============')
//contoh input
var input = [["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]] 
console.log('Output: ')
if (input.length > 0){
	var pangjang = input.length
	for (var i = 0; i < pangjang; i++){		
		console.log("Nomor ID: "+ input[i][0])
		console.log("Nama Lengkap: "+ input[i][1])
		console.log("TTL:  "+ input[i][2])
		console.log("Hobi: "+ input[i][3])
		console.log("")
	}
} else {
	console.log("salah input")
}
console.log('=============')
console.log('Soal No. 5 - (Balik Kata)()')
console.log('=============')
function balikKata(string){
	var KataBalik = '';
	var pangjangKata = (string.length)-1;
	for(var i= 0; pangjangKata >= 0; pangjangKata--){
		KataBalik += string[pangjangKata];
	}
	return KataBalik
	
}
console.log('Output: ')
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

console.log('=============')
console.log('Soal No. 6 - (Metode Array)()')
console.log('=============')
var input6 = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]  
console.log('Output: ')
if (input6.length > 0){
	var pangjang = input6.length
	input6.splice(1,2);
	input6.splice(1,0,"Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung");
	input6.splice(4,0,"Pria");
	input6.splice(5,1);
	input6.splice(5,0,"SMA Internasional Metro");
	console.log(input6)
	var tanggalLahir = input6[3]
	var bulan = tanggalLahir.split("/")
	console.log(bulan[1])
	switch(bulan[1]){
		case '01' : {console.log("Januari"); break;}
		case '02' : {console.log("Februari"); break;}
		case '03' : {console.log("Maret"); break;}
		case '04' : {console.log("April"); break;}
		case '05' : {console.log("Mei"); break;}
		case '06' : {console.log("Juni"); break;}
		case '07' : {console.log("Juli"); break;}
		case '08' : {console.log("Agustus"); break;}
		case '09' : {console.log("September"); break;}
		case '10' : {console.log("Oktober"); break;}
		case '11' : {console.log("November"); break;}
		case '12' : {console.log("Desember"); break;}
		default : {console.log("di luar itu"); break;}
	}
	bulan.sort()
	console.log(bulan)
	var bulan2 = tanggalLahir.split("/")
	var bulann = bulan2.join("-")
	console.log(bulann)
	var nama = input6[1]
	var irisan1 = nama.slice(0,13) 
	console.log(irisan1)
	
} else {
	console.log("salah input")
}
console.log('=============')