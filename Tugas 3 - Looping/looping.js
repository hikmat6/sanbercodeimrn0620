console.log('=============');
console.log('Tugas 3');
console.log('=============');
console.log('Soal No 1 - while');
console.log('=============');
var bilangan = 20;
var flag1 = 1;
var flag2 = 1;
console.log('LOOPING PERTAMA');
while(flag1 <= bilangan) { 
  if(flag1%2==0){
		console.log(flag1 +'- I love coding'); 
	}
  flag1++; 
}
console.log('=============');
console.log('LOOPING KEDUA');
while(bilangan >= flag2) { 
  if(bilangan%2==0){
		console.log(bilangan +'- I will become a mobile developer'); 
	}	
  bilangan--; 
}
console.log('=============');
console.log('Soal No 2 - For');
console.log('=============');
var bilangan2 = 20;
console.log('Output: ');
for(i = 1; i <= bilangan2; i++){
	if(i%2 == 1 && i%3 == 0){
		console.log(i+'- I Love Coding');
	} else if(i%2 == 1){
		console.log(i+'- Santai ');
	} else if(i%2 == 0){
		console.log(i+'- Berkualitas');
	} else {
		console.log(i);
	}
}
console.log('=============');
console.log('Soal No 3 - Membuat Persegi Panjang');
console.log('=============');
var panjang = 8;
var lebar = 4;
console.log('Output: ');
for(i = 1; i <= lebar; i++){
	baris = '';
	for(j = 1; j <= panjang; j++){
		baris = baris +'#';
	}
	console.log(baris);
	baris = '';//resetter
}
console.log('=============');
console.log('Soal No. 4 Membuat Tangga');
console.log('=============');
var dimensi = 7;
console.log('Output: ');
for(i = 1; i <= dimensi; i++){
	baris = '';
	for(j = 1; j <= i; j++){
		baris = baris + '#';
	}
	console.log(baris);
	baris = '';//resetter
}

console.log('=============');
console.log('Soal No. 5 Membuat Papan Catur');
console.log('=============');
var dimensi_papan_catur = 8;
console.log('Output: ');
for(i = 1; i <= dimensi_papan_catur; i++){
	baris = '';
	if(i%2 == 1){
		for(j = 1; j <= dimensi_papan_catur; j++){
			if(j%2 == 1){
			baris = baris + ' ';
			}else{
			baris = baris + '#';
			}
		}
	}else{
		for(j = 1; j <= dimensi_papan_catur; j++){
			if(j%2 == 1){
			baris = baris + '#';
			}else{
			baris = baris + ' ';
			}
		}
	}
	console.log(baris);
	baris = '';//resetter
}

