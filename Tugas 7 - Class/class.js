console.log('=============')
console.log('Tugas 7')
console.log('=============')
console.log('Soal No 1 - Animal Class')
console.log('=============')

class Animal {
  constructor(nickname) {
	this.name = nickname;
	this.legs = 4;
	this.cold_blooded = false;
  }
}

console.log('Release 0')
console.log('=============')
var sheep = new Animal("shaun");
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false
console.log('=============')
console.log('Release 1')
console.log('=============')

class Ape extends Animal{
  constructor(nickname) {
	super()
	this.ape_name = nickname;
  }
  yell(){
	console.log('Auooo')
  }
}

class Frog extends Animal{
  constructor(nickname) {
	super()
	this.frog_name = nickname;	
  }
  jump(){
	console.log('hop hop')
  }
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

console.log('=============')
console.log('Soal No 2 - Function to Class')
console.log('=============')
class Clock {
	constructor(template) {
		this.templateObj = template;	
		this.timer = '';
		this.render2 = function(){
			var date = new Date();
			var template_awal = template.template;
			var hours = date.getHours();
			if (hours < 10) hours = '0' + hours;
			var mins = date.getMinutes();
			if (mins < 10) mins = '0' + mins;
			var secs = date.getSeconds();
			if (secs < 10) secs = '0' + secs;
			var output = template_awal
			  .replace('h', hours)
			  .replace('m', mins)
			  .replace('s', secs);
			console.log(output);
		}
	}
	
	render() {
    var date = new Date();
    var template_awal = this.templateObj.template;

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    var output = template_awal
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
  }

  start() {
    this.render();
    this.timer = setInterval(this.render2, 1000);
  }
}

var clock = new Clock({template: 'h:m:s'});
clock.start();