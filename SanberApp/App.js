import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

// import YoutubeUI from './Tugas/Tugas12/App';
import LoginUI from './Tugas/Tugas13/App';
// import NoteUI from './Tugas/Tugas14/App';
// import NavUI from './Tugas/Tugas15/App';
// import LoginUI from './Tugas/Quiz3';

export default function App() {
  return (
    <LoginUI />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
