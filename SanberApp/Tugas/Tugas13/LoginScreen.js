import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    StatusBar,
    TextInput
} from 'react-native';

export default function LoginScreen() {
    const [text1, setText, text2] = React.useState('');
    return (
        <View>
            <StatusBar></StatusBar>
            <View style={styles.navBar}>
                <Image source={require('./images/logo.png')} style={{ width: 375, height: 116 }} />
            </View>
            <View style={styles.navLogin}>
                <Text style={styles.loginTitle}>Login</Text>
            </View>
            <View style={styles.formInput}>
                <Text style={styles.fieldTitle}>Username / Email</Text>
                <TextInput
                    style={styles.textInputUsername}
                    label="Email"
                    placeholder="Email"
                    placeholderTextColor="#9a73ef"
                    value={text1}
                    onChangeText={text1 => setText(text1)}
                />
                <View style={{ paddingTop: 20 }} />
                <Text style={styles.fieldTitle}>Password</Text>
                <TextInput
                    style={styles.textInputPassword}
                    secureTextEntry={true}
                    label="Password"
                    placeholder="Password"
                    placeholderTextColor="#9a73ef"
                    value={text2}
                    onChangeText={text2 => setText(text2)}
                />
            </View>
            <View style={styles.tombolSubmit}>
                <Text style={styles.submitTitle}>Masuk ?</Text>
            </View>
            <View style={{ marginTop: 20, alignItems: 'center' }}>
                <Text style={styles.atauTitle}>atau</Text>
            </View>
            <View style={styles.tombolDaftar}>
                <Text style={styles.submitTitle}>Daftar</Text>
            </View>

        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    navBar: {
        paddingTop: 63,
        alignItems: 'center'
    },
    navLogin: {
        paddingTop: 55,
        alignItems: 'center',
    },
    formInput: {
        paddingTop: 32,
        paddingLeft: 41,
        paddingRight: 41

    },
    loginTitle: {
        fontFamily: 'sans-serif-light',
        fontSize: 36,
        fontWeight: "bold",
        color: '#003366'
    },
    fieldTitle: {
        fontFamily: 'sans-serif-light',
        fontSize: 16,
        fontWeight: "bold",
        paddingBottom: 5,
        color: '#003366'
    },
    textInputUsername: {
        height: 48,
        borderColor: '#7a42f4',
        borderWidth: 1,
        paddingLeft: 10
    },
    textInputPassword: {
        height: 48,
        borderColor: '#7a42f4',
        borderWidth: 1,
        paddingLeft: 10
    },
    tombolDaftar: {
        marginLeft: 120,
        marginRight: 120,
        marginTop: 20,
        height: 40,
        borderRadius: 16,
        backgroundColor: '#003366',
        alignItems: 'center',
    },
    tombolSubmit: {
        marginLeft: 120,
        marginRight: 120,
        marginTop: 40,
        height: 40,
        borderRadius: 16,
        backgroundColor: '#3EC6FF',
        alignItems: 'center'
    },
    submitTitle: {
        fontFamily: 'sans-serif-light',
        fontSize: 24,
        fontWeight: "bold",
        color: '#FFFFFF'
    },
    atauTitle: {
        fontFamily: 'sans-serif-light',
        fontSize: 24,
        fontWeight: "bold",
        color: '#3EC6FF'
    }
});