import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    StatusBar,
    TextInput
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

export default function AboutUs() {
    return (
        <View style={styles.container}>
            <StatusBar></StatusBar>
            <View style={styles.navTop}>
                <Text style={styles.loginTitle}>Tentang Saya</Text>
                <Icon style={{ marginTop: 10 }} name='account-circle' size={200} />
                <Text style={styles.nameTitle}>Hikmat Hidayat</Text>
                <Text style={styles.devTitle}>React Native Developer</Text>
            </View>
            <View style={styles.folioBox}>
                <Text style={styles.fieldTitle}>Portfolio</Text>
                <View style={styles.border}></View>
                <View style={styles.tabBar}>
                    <View style={styles.tabItem}>
                        <MaterialCommunityIcons name='gitlab' size={40} />
                        <Text style={styles.fieldTitle}>@hikmat6</Text>
                    </View>
                    <View style={styles.tabItem}>
                        <MaterialCommunityIcons name='gitlab' size={40} />
                        <Text style={styles.fieldTitle}>@hikmatcnn</Text>
                    </View>
                </View>
            </View>
            <View style={styles.hubBox}>
                <Text style={styles.fieldTitle}>Hubungi Saya</Text>
                <View style={styles.border}></View>
                <View style={styles.itemHubBox}>
                    <View style={styles.itemiconHubBox}>
                        <MaterialCommunityIcons name='facebook' size={40} style={{ marginRight: 10 }} />
                        <Text style={styles.fieldTitle}>FB hikmat.hidayat</Text>
                    </View>
                    <View style={styles.itemiconHubBox}>
                        <MaterialCommunityIcons name='instagram' size={40} style={{ marginRight: 10 }} />
                        <Text style={styles.fieldTitle}> IG hikmatcnn</Text>
                    </View>
                    <View style={styles.itemiconHubBox}>
                        <MaterialCommunityIcons name='twitter' size={40} style={{ marginRight: 10 }} />
                        <Text style={styles.fieldTitle}>twitter hikmatcnn</Text>
                    </View>
                </View>
            </View>
        </View >
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    navTop: {
        paddingTop: 45,
        alignItems: 'center',
    },
    loginTitle: {
        fontFamily: 'sans-serif-light',
        fontSize: 36,
        fontWeight: "bold",
        color: '#003366'
    },
    nameTitle: {
        fontFamily: 'sans-serif-light',
        fontSize: 24,
        fontWeight: "bold",
        color: '#003366'
    },
    devTitle: {
        marginTop: 8,
        fontFamily: 'sans-serif-light',
        fontSize: 16,
        fontWeight: "bold",
        color: '#3EC6FF'
    },
    folioBox: {
        marginTop: 16,
        paddingTop: 8,
        paddingLeft: 16,
        paddingRight: 16,
        height: 140,
        backgroundColor: '#EFEFEF'
    },
    border: {
        backgroundColor: '#003366',
        height: 1,
    },
    tabBar: {
        height: 72,
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    tabItem: {
        paddingTop: 30,
        alignItems: 'center',
        justifyContent: 'center'
    },
    fieldTitle: {
        fontFamily: 'sans-serif-light',
        fontSize: 16,
        fontWeight: "bold",
        paddingBottom: 5,
        color: '#003366'
    },
    hubBox: {
        marginTop: 16,
        paddingTop: 8,
        paddingLeft: 16,
        paddingRight: 16,
        backgroundColor: '#EFEFEF'
    },
    itemHubBox: {
        paddingTop: 30,
        alignItems: 'center'
    },
    itemiconHubBox: {
        paddingTop: 8,
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingBottom: 8,
    }
});